const company = require('./company');

describe('company', function() {
    it('error when input is string', function() {
        expect(function() {
            company('cat'); 
        }).toThrow('id needs to be integer');
    });

    it('company id 3', () => {
        const test1 = company(3);
        expect(test1).toMatchSnapshot();
    });
    it('comp id 85', () => {
        const test2 = company(85);
        expect(test2).toMatchSnapshot();
    });
});

