function lowercase(word)
{
    if (typeof word !== 'string')
    {
        throw new Error('bad input');
    }
    return word.toLowerCase();
}
module.exports = lowercase;
