/**
 * returns sum of two numbers
 *
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */
function sum (a, b, c = 0) {
    return a + b + c;
}
module.exports = sum;
